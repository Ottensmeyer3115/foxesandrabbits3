import java.util.Iterator;
import java.util.Random;

/**
 * 
 * @author Fredrick Ottensmeyer
 *
 * This class will simulate a deadly small pox virus outbreak during
 * foxes and rabits simulation
 */
public class SmallPox extends Actor {

	private static final Random rand = new Random();


	public SmallPox() {
		 
		// TODO Auto-generated constructor stub
		
	}

	/**
	 * This method overrides method in the superclass.  Acting as 
	 * a method to kill everything!!!!!!!!! :)))))))
	 */
	@Override
	public void act(Field currentField, Field updatedField) {
		// TODO Auto-generated method stub
		Iterator<Location> hosts = currentField.adjacentLocations(getLocation());
		
			while(hosts.hasNext())
			{
				Location where = hosts.next(); // :))))))))))))))))))))))))))
	            Actor animal = currentField.getActorAt(where);  // :D
	            if(animal instanceof Animal)  // :D 
	            {
	            	Animal death = (Animal) animal;  // :D
	            	if(death.isAlive()) {   // :D
	                    death.setDead(); // :)))))))))
	                    SmallPox smallpox = new SmallPox();
	                    smallpox.setLocation(where);  // :))))))))))
	                    updatedField.place(smallpox);  // sets location
	            }
			}
		

	}
}
}