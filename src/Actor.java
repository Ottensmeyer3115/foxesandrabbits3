
public abstract class Actor {

	//Location of the actor
	private Location location;
	
	public Actor()
	{
		
	}
	
	abstract public void act(Field currentField, 
            Field updatedField);
	
	 /**
     * Return the actors location.
     * @return The actors location.
     */
    public Location getLocation()
    {
        return location;
    }

    /**
     * Set the actors location.
     * @param row The vertical coordinate of the location.
     * @param col The horizontal coordinate of the location.
     */
    public void setLocation(int row, int col)
    {
        this.location = new Location(row, col);
    }

    /**
     * Set the actors location.
     * @param location The actors location.
     */
    public void setLocation(Location location)
    {
        this.location = location;
    }
}
